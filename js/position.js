const PositionController = {

  state: {
    positions: [
      /* iron condor */
      { isBuy: 1, isCall: 0, expiry: '2022-02-25', strike: 30000, premium: 164, quantity: 1, greeks: {}},
      { isBuy: 0, isCall: 0, expiry: '2022-02-25', strike: 35000, premium: 1015, quantity: 1, greeks: {}},
      { isBuy: 0, isCall: 1, expiry: '2022-02-25', strike: 40000, premium: 1836, quantity: 1, greeks: {}},
      { isBuy: 1, isCall: 1, expiry: '2022-02-25', strike: 45000, premium: 447, quantity: 1, greeks: {}},

      /* long put condor */
      /*{ isBuy: 1, isCall: 0, expiry: '2022-02-25', strike: 30000, premium: 207, quantity: 1, greeks: {}},
      { isBuy: 0, isCall: 0, expiry: '2022-02-25', strike: 33000, premium: 748, quantity: 1, greeks: {}},
      { isBuy: 0, isCall: 0, expiry: '2022-02-25', strike: 40000, premium: 4200, quantity: 1, greeks: {}},
      { isBuy: 1, isCall: 0, expiry: '2022-02-25', strike: 44000, premium: 7300, quantity: 1, greeks: {}},*/

    ],
    totalPremiums: 0,
    coinPrice: 38000,
    coinSelector: 'custom'
  },

  addPosition() {
    this.state.positions.push(
      { isBuy: 0, isCall: 1, expiry: '2022-02-25', strike: 40000, premium: 1836, quantity: 1, greeks: {}},
    );

    // initialize date picker
    this.$nextTick(() => flatpickr('.datepicker'));

    this.onPositionChange();
  },

  removePosition(index) {
    this.state.positions.splice(index, 1);

    this.onPositionChange();
  },

  getTotalPremiums() {
    var n = 0;

    this.state.positions.forEach(p => {
      isBuySign = p.isBuy ? -1 : 1;
      n += Number(p.premium) * Number(p.quantity) * isBuySign;
    });

    return n;
  },

  calculatePL(n, t, i, r) {
    var total = 0;

    //n is price, t is day, i is volatility, r is rate
    //console.log(`calculatePL - n: ${n}, t: ${t}, i: ${i}, r: ${r}`);

    // loop through positions and calculate PL for each
    this.state.positions.forEach(p => {

      var daysToExpiry = Math.ceil((new Date(p.expiry).getTime() - new Date().getTime()) / 86400000);

      var f = daysToExpiry - t;

      if (f < 0) PL = 0;
      else {
        var o = f / 365, // yearsToExpiry divide by 365

        optionBS = new BSHolder(n, p.strike, r, i, o);
        premiumBS = p.isCall ? BS['call'](optionBS) : BS['put'](optionBS);

        isBuySign = p.isBuy ? 1 : -1
        PL = p.quantity * premiumBS * isBuySign; // quantity x premium x +/-
      }

      total += PL;
    });

    return total;
  },

  getStrikes() {
    var n = [];
    this.state.positions.forEach(p => {
      n.push(Number(p.strike));
    });

    return n;
  },
  getMinStrike() {
    return Math.min.apply(Math, this.getStrikes());
  },
  getMaxStrike() {
    return Math.max.apply(Math, this.getStrikes());
  },
  getDistanceBetweenMinMaxStrikes() {
    return this.getMaxStrike() - this.getMinStrike()
  },

  getMinDaysToExpiry() {
    return Math.ceil((new Date(this.getMinExpiry()).getTime() - new Date().getTime()) / 86400000);
  },
  getMaxDaysToExpiry() {
    return Math.ceil((new Date(this.getMaxExpiry()).getTime() - new Date().getTime()) / 86400000);
  },
  getMinExpiry() {
    return Math.min.apply(Math, this.getExpiries());
  },
  getMaxExpiry() {
    return Math.max.apply(Math, this.getExpiries());
  },
  getExpiries() {
    var n = [];

    this.state.positions.forEach(p => {
      n.push(new Date(p.expiry).getTime());
    });

    return n;
  },

  onPositionChange() {
    var underlying_price = this.state.coinPrice;

    this.state.positions.forEach(p => {

      yearsToExpiry = Math.ceil((new Date(p.expiry).getTime() - new Date().getTime()) / 86400000) / 365;
      
      var iv = BS.getImpliedVolatility(p.premium, underlying_price, p.strike, yearsToExpiry, 0.02, p.isCall);
      p.iv = Number(iv * 100).toFixed(2) + '%';

      optionBS = new BSHolder(underlying_price, p.strike, 0.02, iv, yearsToExpiry);

      greeks = {};
      if(p.isCall) {
        //greeks['premium'] = BS['call'](optionBS);
        greeks['delta'] = 'delta = '+ BS['cdelta'](optionBS).toFixed(5);
        greeks['theta'] = 'theta = '+ BS['ctheta'](optionBS).toFixed(5);
        greeks['rho'] = 'rho = '+ BS['crho'](optionBS).toFixed(5);
      } else {
        //greeks['premium'] = BS['put'](optionBS);
        greeks['delta'] = 'delta = '+ BS['pdelta'](optionBS).toFixed(5);
        greeks['theta'] = 'theta = '+ BS['ptheta'](optionBS).toFixed(5);
        greeks['rho'] = 'rho = '+ BS['prho'](optionBS).toFixed(5);
      }

      greeks['gamma'] = 'gamma = '+ BS['gamma'](optionBS).toFixed(5);
      greeks['vega'] = 'vega = '+ BS['vega'](optionBS).toFixed(5);

      // greeks
      p.greeks = greeks;

      // amount
      isBuySign = p.isBuy ? -1 : 1;
      p.amount = Number(p.premium * p.quantity * isBuySign).toFixed(2);

    });

    this.state.totalPremiums = Number(this.getTotalPremiums()).toFixed(2);

    runChart();
  },

  updateCoinPrice() {
    coin = this.state.coinSelector;
    console.log(coin);

    if(coin == 'custom') {
        document.getElementById('priceInput').disabled = false;

        this.onPositionChange();
    } else {
        (async () => {
            const response = await fetch('https://api.coingecko.com/api/v3/simple/price?ids=bitcoin,ethereum,solana&vs_currencies=usd')
            if (response.ok) {
                data = await response.json();

                document.getElementById('priceInput').disabled = true;
                this.state.coinPrice = data[coin]['usd'];
            } else {
                document.getElementById('priceInput').disabled = false;
                this.state.coinSelector = 'custom';  
            }
            
            this.onPositionChange();
        })()
    }
  },

  addStrategyPositions(steps) {
    var today = new Date();
    var nextWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 7)
                    .toISOString().slice(0, 10);
    var next2Weeks = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 14)
                    .toISOString().slice(0, 10);
    var next3Weeks = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 21)
    .toISOString().slice(0, 10);

    console.log(steps);
    /*var steps = [
      //[isBuy, isCall, week to expiry, strike, quantity]
      [1, 0, 1, 0.75, 1],
      [0, 0, 1, 0.85, 1],
      [0, 0, 1, 1.15, 1],
      [1, 0, 1, 1.25, 1],
    ];*/

    var vola = 0.65;
    var interest = 0.02;

    var _positions = [];


    for(step in steps) {
      var p = {};

      p.isBuy = steps[step][0];
      p.isCall = steps[step][1];

      p.expiry = nextWeek, term = 7 / 365; // if(step[2] == 1)
      if(steps[step][2] == 2) p.expiry = next2Weeks, term = 14 / 365;
      if(steps[step][2] == 3) p.expiry = next3Weeks, term = 21 / 365;
      
      p.strike = this.state.coinPrice * steps[step][3];

      optionBS = new BSHolder(this.state.coinPrice, p.strike, interest, vola, term);
      p.premium = p.isCall ? BS['call'](optionBS) : BS['put'](optionBS);

      p.quantity = steps[step][4];

      _positions.push(p);
    }
    
    /*_positions = [
      { isBuy: 1, isCall: 0, expiry: '2022-02-25', strike: 30000, premium: 207, quantity: 1, greeks: {}},
      { isBuy: 0, isCall: 0, expiry: '2022-02-25', strike: 33000, premium: 748, quantity: 1, greeks: {}},
      { isBuy: 0, isCall: 0, expiry: '2022-02-25', strike: 40000, premium: 4200, quantity: 1, greeks: {}},
      { isBuy: 1, isCall: 0, expiry: '2022-02-25', strike: 44000, premium: 7300, quantity: 1, greeks: {}},
    ];*/

    /*_positions.push(
      { isBuy: 1, isCall: 0, expiry: '2022-02-25', strike: 30000, premium: 164, quantity: 1, greeks: {}}
    );*/

    this.state.positions = _positions;

    this.onPositionChange();
  }


};