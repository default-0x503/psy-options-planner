var guides = {
  'long-call': {
    'mainText': `A simple bullish strategy for beginners that can yield big rewards. A call gives the buyer the right, but not the obligation, to buy the underlying stock at strike price A. However, you can simply buy and sell a call before it expires to profit off the price change.
                  The value of the option will decay as time passes, and is sensitive to changes in volatility. Your maximum loss is capped at the price you pay for the option.`,
    'image': '',
    'tags': '[Bullish] [Unlimited Profit] [Limited Loss]',
    'steps': '1. Buy a call at strike A',
    '_steps': [ [1, 1, 1, 1.15, 1] ],
  },
  'long-put': {
    'mainText': `A simple bearish strategy for beginners that can yield big rewards. A put gives the buyer the right, but not the obligation, to sell the underlying stock at strike price A. However, you can simply buy and sell a put before it expires to profit off the price change.
                  The value of the option will decay as time passes, and is sensitive to changes in volatility. Your maximum loss is capped at the price you pay for the option.`,
    'image': '',
    'tags': '[Bearish] [Nearly Unlimited Profit] [Limited Loss]',
    'steps': '1. Buy a put at strike A',
    '_steps': [ [1, 0, 1, 0.85, 1] ],
  },
  'short-call': {
    'mainText': `A simple but risky strategy which results in an initial credit. By selling a call, you are liable to sell 100 shares of the underlying stock at strike price A if assigned. Because of this, you should have the cash or stock to cover such a situation. If it expires below strike A, you simply keep the full credit.<br/>(also known as: Naked Call)`,
    'image': '',
    'tags': '[Bearish] [Limited Profit] [Limited Loss]',
    'steps': '1. Sell a call at strike A',
    '_steps': [ [0, 1, 1, 1.15, 1] ],
  },
  'short-put': {
    'mainText': `A simple but risky strategy which results in an initial credit. By selling a put, you are liable to buy 100 shares of the underlying stock at strike price A if assigned. Because of this, you should be okay with buying the stock at such a price. If it expires above strike A, you simply keep the full credit.<br/>(also known as: Naked Put)`,
    'image': '',
    'tags': '[Bullish] [Limited Profit] [Nearly Unlimited Loss]',
    'steps': '1. Sell a put at strike A',
    '_steps': [ [0, 0, 1, 0.85, 1] ],
  },  


  'bull-put-spread': {
    'mainText': `A bullish vertical spread strategy which has limited risk and reward. It combines a long and short put which caps the upside, but also the downside. The goal is for the stock to be above strike B, which allows both puts to expire worthless. This strategy is almost neutral to changes in volatility. Time-decay is helpful while it is profitable, but harmful when it is losing.<br/>(also known as: Put Credit Spread)`,
    'image': '',
    'tags': '[Bullish] [Limited Profit] [Limited Loss]',
    'steps': '1. Buy a put at strike A<br>2. Sell a put at strike B',
    '_steps': [ [1, 0, 1, 0.85, 1], [0, 0, 1, 1.15, 1] ],
  },
  'bear-call-spread': {
    'mainText': `A bearish vertical spread strategy which has limited risk and reward. It combines a short and a long call which caps the upside, but also the downside.<br/>The goal is for the stock to be below strike A, which allows both calls to expire worthless. This strategy is almost neutral to changes in volatility. Time-decay is helpful while it is profitable, but harmful when it is losing.<br/>(also known as: Call Credit Spread)`,
    'image': '',
    'tags': '[Bearish] [Limited Profit] [Limited Loss]',
    'steps': '1. Sell a call at strike A<br>2. Buy a call at strike B',
    '_steps': [ [0, 1, 1, 0.85, 1], [1, 1, 1, 1.15, 1] ],
  },


  'iron-butterfly': {
    'mainText': `An iron butterfly has similar characteristics to a put or call butterfly, but is established as a net credit. It is made of a combination of a bull put spread and a bear call spread. Volatility should be low to run this strategy, as increasing volatility will narrow the profitable range. Time is helpful when the position is profitable, and harmful when it isn't.<br>(also known as: Short Iron Butterfly)`,
    'image': '',
    'tags': '[Neutral] [Limited Profit] [Limited Loss]',
    'steps': '1. Buy a put at strike A<br>2. Sell a put at strike B<br>3. Sell a call at strike B<br>4. Buy a call at strike C',
    '_steps': [ [1, 0, 1, 0.75, 1], [0, 0, 1, 0.85, 1], [0, 1, 1, 0.85, 1], [1, 1, 1, 1.15, 1]],
  },
  'iron-condor': {
    'mainText': `An iron condor is a neutral strategy that is profitable if the stock remains within the inner strikes B and C. It is established for a net credit and has a wider profitable range than an iron butterfly, but the potential profit is lower. Volatility should be low to run this strategy, as increasing volatility will narrow the profitable range. Time is helpful when the position is profitable, and harmful when it isn't.<br>(also known as: Short Iron Condor)`,
    'image': '',
    'tags': '[Neutral] [Limited Profit] [Limited Loss]',
    'steps': '1. Buy a put at strike A<br>2. Sell a put at strike B<br>3. Sell a call at strike C<br>4. Buy a call at strike D',
    '_steps': [ [1, 0, 1, 0.75, 1], [0, 0, 1, 0.85, 1], [0, 1, 1, 1.15, 1], [1, 1, 1, 1.25, 1]],
  }, 

  'iron-butterfly-inverse': {
    'mainText': `The opposite of a iron butterfly. This strategy is a net debit and unlike the long butterfly, it doesn't offer a very risk/reward ratio. If the stock moves in either direction you can net a small profit. If volatility increases, your chance of profit will increase as well. Time is harmful when the position is unprofitable, but helpful once it becomes profitable.<br>(also known as: Long Iron Butterfly)`,
    'image': '',
    'tags': '[Directional] [Limited Profit] [Limited Loss]',
    'steps': '1. Buy a put at strike A<br>2. Sell a put at strike B<br>3. Sell a call at strike B<br>4. Buy a call at strike C',
    '_steps': [ [1, 0, 1, 0.85, 1], [0, 0, 1, 1.15, 1], [0, 1, 1, 1.15, 1], [1, 1, 1, 1.15, 1]],
  },
  'iron-condor-inverse': {
    'mainText': `The opposite of a iron condor, and similar to a short iron butterfly, but with a wider unprofitable range and better potential profit. If volatility increases, your chance of profit will increase as well. Time is harmful when the position is unprofitable, but helpful once it becomes profitable.<br>(also known as: Long Iron Condor)`,
    'image': '',
    'tags': '[Directional] [Limited Profit] [Limited Loss]',
    'steps': '1. Sell a put at strike A<br>2. Buy a put at strike B<br>3. Buy a call at strike C<br>4. Sell a call at strike D',
    '_steps': [ [0, 0, 1, 0.75, 1], [1, 0, 1, 0.85, 1], [1, 1, 1, 1.15, 1], [0, 1, 1, 1.25, 1]],
  },


  'long-call-butterfly': {
    'mainText': `A call butterfly spread is the combination of a bull call spread and a bear call spread. This creates a neutral strategy that is cheap and has a good risk/reward ratio.
      <br>Volatility should be low to run this strategy, as increasing volatility will narrow the profitable range. Time is helpful when the position is profitable, and harmful when it isn't.`,
    'image': '',
    'tags': '[Neutral] [Limited Profit] [Limited Loss]',
    'steps': '1. Buy a call at strike A<br>2. Sell two calls at strike B<br>3. Buy a call at strike C',
    '_steps': [ [1, 1, 1, 0.75, 1], [0, 1, 1, 0.85, 1], [0, 1, 1, 0.85, 1], [1, 1, 1, 1.15, 1] ],
  },
  'long-put-butterfly': {
    'mainText': `A put butterfly spread is the combination of a bull put spread and a bear put spread. This creates a neutral strategy that is cheap and has a good risk/reward ratio.
      <br>Volatility should be low to run this strategy, as increasing volatility will narrow the profitable range. Time is helpful when the position is profitable, and harmful when it isn't.`,
    'image': '',
    'tags': '[Neutral] [Limited Profit] [Limited Loss]',
    'steps': '1. Buy a put at strike A<br>2. Sell two puts at strike B<br>3. Buy a put at strike C',
    '_steps': [ [1, 0, 1, 0.75, 1], [0, 0, 1, 0.85, 1], [0, 0, 1, 0.85, 1], [1, 0, 1, 1.15, 1] ],
  },

  'short-call-butterfly': {
    'mainText': `A short call butterfly is a volatility strategy that can be profitable if there is a big move in either direction. It is the opposite of a long call butterfly.
      <br>Increasing volatility or a big move is required for this strategy to become profitable. Time is harmful when the position is unprofitable, but helpful once it becomes profitable.`,
    'image': '',
    'tags': '[Directional] [Limited Profit] [Limited Loss]',
    'steps': '1. Sell a call at strike A<br>2. Buy two calls at strike B<br>3. Sell a call at strike C',
    '_steps': [ [0, 1, 1, 0.75, 1], [1, 1, 1, 0.85, 1], [1, 1, 1, 0.85, 1], [0, 1, 1, 1.15, 1] ],
  },
  'short-put-butterfly': {
    'mainText': `A short put butterfly is a volatility strategy that can be profitable if there is a big move in either direction. It is the opposite of a long put butterfly.
      <br>Increasing volatility or a big move is required for this strategy to become profitable. Time is harmful when the position is unprofitable, but helpful once it becomes profitable.`,
    'image': '',
    'tags': '[Directional] [Limited Profit] [Limited Loss]',
    'steps': '1. Sell a put at strike A<br>2. Buy two puts at strike B<br>3. Sell a put at strike C',
    '_steps': [ [0, 0, 1, 0.75, 1], [1, 0, 1, 0.85, 1], [1, 0, 1, 0.85, 1], [0, 0, 1, 1.15, 1] ],
  },


  'calendar-call-spread': {
    'mainText': `A neutral to mildly bearish/bullish strategy using two calls of the same strike, but different expiration dates. If the stock is near strike A when the earlier call expires, you will be able to close it for a profit.
      <br>Use an at-the-money strike to make this strategy neutral, or a slightly out-of-the-money or in-the-money strike to give a bullish or bearish bias.
      <br>(also known as: Horizontal Call Spread)`,
    'image': '',
    'tags': '[Neutral] [Limited Profit] [Limited Loss]',
    'steps': '1. Sell a call at strike A<br>2. Buy a call at strike A (further expiration)',
    '_steps': [ [0, 1, 1, 1.15, 1], [1, 1, 2, 1.15, 1] ],
  },
  'calendar-put-spread': {
    'mainText': `A neutral to mildly bearish/bullish strategy using two puts of the same strike, but different expiration dates. If the stock is near strike A when the earlier call expires, you will be able to close it for a profit.
      <br>Use an at-the-money strike to make this strategy neutral, or a slightly out-of-the-money or in-the-money strike to give a bullish or bearish bias.
      <br>(also known as: Horizontal Put Spread)`,
    'image': '',
    'tags': '[Neutral] [Limited Profit] [Limited Loss]',
    'steps': '1. Sell a put at strike A<br>2. Buy a put at strike A (further expiration)',
    '_steps': [ [0, 0, 1, 0.85, 1], [1, 0, 2, 0.85, 1] ],
  },

  'diagonal-call-spread': {
    'mainText': `A variation of the calendar spread where the long (later expiration) call is further in the money, which changes the shape of the risk profile.
      <br>(also known as: Poor Man's Covered Call)`,
    'image': '',
    'tags': '[Bullish] [Limited Profit] [Limited Loss]',
    'steps': '1. Buy a call at strike A (further expiration)<br>2. Sell a call at strike B',
    '_steps': [ [1, 1, 2, 1.15, 1], [0, 1, 1, 1.25, 1] ],
  },
  'diagonal-put-spread': {
    'mainText': `A variation of the calendar spread where the long (later expiration) put is further in the money, which changes the shape of the risk profile.`,
    'image': '',
    'tags': '[Bearish] [Limited Profit] [Limited Loss]',
    'steps': '1. Sell a put at strike A<br>2. Buy a put at strike B (further expiration)',
    '_steps': [ [0, 0, 1, 0.85, 1], [1, 0, 2, 0.7, 1] ],
  },


  'short-straddle': {
    'mainText': `The opposite of a long straddle. This strategy makes good income since a put and call are being sold, but requires minimal stock movement as the max loss is uncapped in both directions.
      <br>Time is beneficial for this strategy as both options will decay and become cheaper to buy back, but since there is unlimited risk you also don't want to be exposed for too long.`,
    'image': '',
    'tags': '[Neutral] [Limited Profit] [Unlimited Loss]',
    'steps': '1. Sell a put at strike A<br>2. Sell a call at strike A',
    '_steps': [ [0, 0, 1, 0.85, 1], [0, 1, 1, 0.85, 1] ],
  },
  'short-strangle': {
    'mainText': `The opposite of a long strangle, and similar to a short straddle but with different strikes. This strategy makes less income than a short straddle, but also has a wider profitable range, making the worst case scenario less likely.
      <br>Time is beneficial for this strategy as both options will decay and become cheaper to buy back, but like a short straddle, there is unlimited risk so you don't want to be exposed for too long.`,
    'image': '',
    'tags': '[Neutral] [Limited Profit] [Unlimited Loss]',
    'steps': '1. Sell a put at strike A<br>2. Sell a call at strike B',
    '_steps': [ [0, 0, 1, 0.85, 1], [0, 1, 1, 1.15, 1] ],
  },

  'long-put-condor': {
    'mainText': `Similar to a long put butterfly, but the middle options have different strikes. It has a wider profitable range than a long put butterfly, but the potential profit is lower and the maximum loss is higher.`,
    'image': '',
    'tags': '[Neutral] [Limited Profit] [Limited Loss]',
    'steps': '1. Buy a put at strike A<br>2. Sell a put at strike B<br>3. Sell a put at strike C<br>4. Buy a put at strike D',
    '_steps': [ [1, 0, 1, 0.75, 1], [0, 0, 1, 0.85, 1], [0, 0, 1, 1.15, 1], [1, 0, 1, 1.25, 1] ],
  },
  'long-call-condor': {
    'mainText': `Similar to a long call butterfly, but the middle options have different strikes. It has a wider profitable range than a long call butterfly, but the potential profit is lower and the maximum loss is higher.`,
    'image': '',
    'tags': '[Neutral] [Limited Profit] [Limited Loss]',
    'steps': '1. Buy a call at strike A<br>2. Sell a call at strike B<br>3. Sell a call at strike C<br>4. Buy a call at strike D',
    '_steps': [ [1, 1, 1, 0.75, 1], [0, 1, 1, 0.85, 1], [0, 1, 1, 1.15, 1], [1, 1, 1, 1.25, 1] ],
  },
}