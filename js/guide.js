const GuideController = {
  state: {
    mainText: guides['long-call'].mainText,
    tags: guides['long-call'].tags,
    steps: guides['long-call'].steps,
    image: '',
    selected: 'long-call',
  },

  changeGuide() {
    var selected = this.state.selected;
    this.state.mainText = guides[selected].mainText;
    this.state.tags = guides[selected].tags;
    this.state.steps = guides[selected].steps;

  },

  applyGuide($dispatch) {
    $dispatch('strategy', { steps: guides[this.state.selected]._steps });

    UIkit.modal('#modal-guides').hide();
    console.log(`applyGuide: ${this.state.selected}`);
  }
}