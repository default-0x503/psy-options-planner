Charting = function() {
  this.price = 38000;
  this.riskFreeRate = 0.02;
  this.DEFAULT_X_POINTS_NUMBER = 35;
  this.chartVolatility = 0.3;
  this.chartRiskFreeRate = 0.02;
  this.chartDaysFromToday = 0;
  this.xMin = 0;
  this.xMax = 0;

  this.calculate = function() {
      this.setXAxisMinMax();
      this.checkChartDaysFromToday()
  };

  this.checkChartDaysFromToday = function() {
      var n = PositionController.getMaxDaysToExpiry(); // here
      this.chartDaysFromToday.value > n && (this.chartDaysFromToday.value = n - 1e-7)
  };

  this.setXAxisMinMax = function() {
    var minStrike = PositionController.getMinStrike();
    var maxStrike = PositionController.getMaxStrike();

    var extraSpace = (minStrike - maxStrike === 0) ? minStrike * 0.03 : (maxStrike - minStrike) * 0.2;

    this.xMin = minStrike - extraSpace;
    this.xMax = maxStrike + extraSpace;
  };

  this.createXYPoints = function(n, t, i, r, u, f) {
      var s = [], // array of x y
          h, e, a;

      //console.log(`createXYPoints - n: ${n}, t: ${t}, i: ${i}, r: ${r}, u: ${u}, f: ${f}`);
      t === undefined && (t = this.chartDaysFromToday);
      i === undefined && (i = this.chartVolatility);
      r === undefined && (r = this.chartRiskFreeRate);
      n === undefined && (n = 1); //PL
      u === undefined && (u = this.createPricesXArrayWithStrikes());
      f === undefined && (f = 4); //decimal places
      h = PositionController.getTotalPremiums() * -1;

      //console.log(`createXYPoints - n: ${n}, t: ${t}, i: ${i}, r: ${r}, u: ${u}, f: ${f}, h: ${h}`);
      for (e = 0; e < u.length; e++) {

          var c = u[e],
              v = parseFloat(c.toFixed(f)),
              l = PositionController.calculatePL(c, t, i, r),
              o = l;
          
          n === 1 && (a = l - h, o = a);
          o = parseFloat(o.toFixed(f));
          s.push([v, o]);
      }
      return s
  };

  this.createXyGraphArrayAtExpiry = function() {
      var n = PositionController.getMinDaysToExpiry(),
          t = n - 1e-6;
      return this.createXYPoints(1, t)
  };

  this.createXyGraphArrayToday = function() {
    return this.createXYPoints()
  };

  this.createPricesXArrayWithStrikes = function() {
      var n = this.getXStep(this.xMin, this.xMax),
          t = PositionController.getStrikes();

      //console.log(`createPricesXArrayWithStrikes - ${this.xMin}, ${this.xMax}, ${n}, ${t}`);
      return this.createXPointsArray(this.xMin, this.xMax, n, t)
  };

  this.createXPointsArray = function(n, t, i, r) {
    for (var u = [], o = n, e, f; o < t;) u.push(o), o += i;
    if (u.push(t), r.constructor === Array)
        for (e = 0; e < r.length; e++) f = r[e], f > n && f < t && u.indexOf(f) === -1 && u.push(f);
    
    u.sort(function(n, t) { return n - t});

    return u;
  };

  this.getXStep = function(n, t, i) {
      i || (i = this.DEFAULT_X_POINTS_NUMBER);

      // (max with space - min with space) / 35
      var r = (t - n) / i;
      return r
  }

  this.drawChart = function() {

    var lineAtDay = this.createXyGraphArrayToday();
    var lineAtExpiry = this.createXyGraphArrayAtExpiry();
  
    var labelAtDay, chartDaysFromToday = this.chartDaysFromToday, labelAtExpiry;
  
  
    labelAtDay = chartDaysFromToday === 0 ? "Today" : "In " + this.chartDaysFromToday + " Days";
    labelAtExpiry = "At Expiry";
  
  
    chart = new Highcharts.Chart({
        chart: {
            renderTo: "highChartsContainer",
            zoomType: "xy"
        },
        title: {
            text: null
        },
        xAxis: {
            title: {
                enabled: !0,
                text: "Price"
            },
            minPadding: 0,
            maxPadding: 0,
            startOnTick: !1,
            endOnTick: !1,
            showLastLabel: !0
        },
        yAxis: {
            title: {
                text: "Profit / Loss"
            },
            minorGridLineWidth: "0px",
            plotLines: [{
                color: "rgb(180, 180, 180)",
                width: 2,
                value: 0,
                zIndex: 1
            }]
        },
        legend: {
            layout: "vertical",
            align: "left",
            verticalAlign: "top",
            x: 100,
            y: 70,
            floating: !0,
            backgroundColor: Highcharts.theme && Highcharts.theme.legendBackgroundColor || "#FFFFFF",
            borderWidth: 1,
        },
        plotOptions: {
            scatter: {
                animation: !1,
                lineWidth: 3,
                allowPointSelect: !0,
                marker: {
                    radius: 0,
                    states: {
                        hover: {
                            enabled: !0,
                            lineColor: "rgb(100,100,100)"
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: !1
                        }
                    }
                },
                tooltip: {
                    headerFormat: "<b>{series.name}<\/b><br>",
                    pointFormat: "Price = {point.x}<br />Profit / Loss = {point.y}"
                }
            },
            area: {
                animation: !1,
                marker: {
                    radius: 0,
                    states: {
                        hover: {
                            enabled: !0,
                            lineColor: "rgb(100,100,100)"
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: !1
                        }
                    }
                },
                tooltip: {
                    headerFormat: "<b>@ ${point.x}<\/b><br>",
                    pointFormat: "P/L = ${point.y}"
                }
            }
        },
        series: [
            /*{
                name: labelAtDay,
                data: lineAtDay,
                type: 'scatter',
            },*/ {
                name: labelAtExpiry,
                data: lineAtExpiry,
                type: 'area',
                negativeColor: {
                    linearGradient: [0, 0, 0, 300],
                    stops: [
                        [0, 'rgba(255, 0, 0, 0.7)'],
                        [1, 'rgba(255, 0, 0, 0.2)']
                    ]
                },
                color: {
                    linearGradient: [0, 0, 0, 300],
                    stops: [
                        [0, 'rgba(0, 136, 255, 0.7)'],
                        [1, 'rgba(0, 136, 255, 0.2)']
                    ]
                }
            }
        ]
    })
  }
  
}

var charting = new Charting();

function runChart() {
    charting.calculate();
    charting.drawChart();
}