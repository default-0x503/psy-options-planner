## Inspiration
Using a right tool can bring better efficiency and results. I am motivated to build an easy-to-use tool for PsyOptions users to buy and sell options better.

## What it does
1. The tool enable users to add multiple **buy/sell** and **call/put** options at various strikes and expiry dates to visualize PnL of their options strategies.
2. The tool also offers informative guides on various options strategies for users to apply the right option strategies that fits into their outlook of the market.
3. The tool also allows users to visualize positioning of their strategies on a spectrum.

## How we built it
I learned about options trading and researched existing tools for inspiration. Then I created the planner in a simple way with easy-to-use UX.

## Challenges we ran into
I set a requirement to build a simple versatile planner which users can run on their own without coming to a centralize site. I needed to learn about building the interface in a reactive way (Alpine.js). It is a challenge but also a joy for me.

## Accomplishments that we're proud of
I managed to build a ready-to-use planning within the timeline [https://psy-options-planner.netlify.app](https://psy-options-planner.netlify.app)

## What we learned
1. options trading is powerful with right strategies (of course, and a planner)
2. PsyOptions can acquire and on-board new groups of users from DeFi to hedge their holding, staking and impermanent losses from liquidity pools
3. options instruments will drive the next generation of DeFi innovations

## What's next for PsyOptions - Options Planner
1. launch, get feedbacks and improve
2. integrate into PsyOptions frontend
3. build UI and UX for specific use-cases (i.e hedging impermanent loss etc)